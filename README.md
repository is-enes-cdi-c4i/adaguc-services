# ADAGUC services

- ADAGUC-services is a JAVA spring boot software package build to run adaguc-server
- ADAGUC-server is a C++ GIS application to visualize netCDF files via the web. The software provides several features to access and visualize data over the web, it uses OGC standards for data dissemination.

## Start adaguc-services via docker-compose

*Pre-requisites:* Install Docker and docker-compose and make sure you can access the docker deamon as local user ([manual for installation](https://docs.docker.com/v17.09/engine/installation/linux/docker-ce/ubuntu/#set-up-the-repository); [manual for post installation steps](https://docs.docker.com/install/linux/linux-postinstall/). )

To start adaguc-services via docker-compose make the following steps:
- clone this repository and cd into adaguc-services Docker folder
```
cd adaguc-services/Docker
```
- An `.env` environment file is used by docker-compose.yml. A default file can be generated with the command `bash docker-compose-generate-env.sh`. This will setup the default settings for your services/networks/volumes.
```
bash docker-compose-generate-env.sh 
cat .env
```
- Create the listed directories (ADAGUC_DATA_DIR, ADAGUC_AUTOWMS_DIR, ADAGUC_DATASET_DIR, ADAGUCSERVICES_DIR) before starting adaguc:
```
source .env && sudo mkdir -p ${ADAGUC_DATA_DIR} ${ADAGUC_AUTOWMS_DIR} ${ADAGUC_DATASET_DIR} ${ADAGUCSERVICES_DIR}
```
- With `docker-compose up` the application is build, (re)created, started, and attached to containers for a service.
```
docker-compose up --build
```


### Using the basket 
To initialize a user for the basket, first make a listing of it by calling it's API, this will initialize the user `myuser`. In the default configuration you are signed in as myuser, you don't have to sign in.
- Make a basket listing: ``curl -kL https://`hostname`/adaguc-services/basket/list && echo``
- Test the basket: put a TXT file in the basket:

  - From the Docker directory source the environment file, this will set the right environment variables: `source .env`
  - By default the directories are owned by root, change this via `sudo chown $USER: ${ADAGUCSERVICES_DIR}/ -R`
  - Put a file in the basket: `echo "hello!" > ${ADAGUCSERVICES_DIR}/myuser/data/hello.txt`
  - List the basket again: ``curl -kL https://`hostname`/adaguc-services/basket/list | python -m json.tool``
  - Download the file: ``curl -kL https://`hostname`/adaguc-services//opendap/myuser/hello.txt``

  
- Put a NetCDF file in the basket:
  - Download NC file and put it in the basket: `curl -L http://opendap.knmi.nl/knmi/thredds/fileServer/IS-ENES/TESTSETS/tasmax_day_EC-EARTH_rcp26_r8i1p1_20060101-20251231.nc > ${ADAGUCSERVICES_DIR}/myuser/data/netcdf.nc`
  - List basket: ``curl -kL https://`hostname`/adaguc-services/basket/list | python -m json.tool``
  - ncdump the file: ``ncdump -h https://`hostname`/adaguc-services/opendap/myuser/netcdf.nc``
  - ncview the file: ``ncview https://`hostname`/adaguc-services/opendap/myuser/netcdf.nc``


### Getting metadata and previews for files served via OpenDAP  

Adaguc-server is connected to adaguc-services via the wms? servlet endpoint. You can use adaguc-server to list metadata, use the Web Coverage Service (WCS) to download, regrid and reformat the data, use the Web Map Service (WMS) to visualize the data. The WMS, WCS and METADATA endpoints are:
- ```https://`hostname`/adaguc-services/wms?source=${encodedfilename}&service=wms&```
- ```https://`hostname`/adaguc-services/adagucserver?source=${encodedfilename}&service=wcs&```
- ```https://`hostname`/adaguc-services/adagucserver?source=${encodedfilename}&service=metadata&```

For example:
  - Get URLEncode for bash: `curl -L https://gist.githubusercontent.com/cdown/1163649/raw/356166e6a1564d93e02e174718eb59f50108a7aa/gistfile1.sh > /tmp/urlencode.sh` and activate it: `source /tmp/urlencode.sh`
  - Encode the OpenDAP URL: ```encodedfilename=`urlencode "https://\`hostname\`/adaguc-services//opendap/myuser/netcdf.nc"` && echo $encodedfilename```
  - With the encoded URL we can visualize the file and obtain metadata of the file
  - ```echo https://`hostname`/adaguc-services/wms?source=${encodedfilename}``` You can use this URL in https://geoservices.knmi.nl/viewer3.0/ to visualize the data. You can also use this URL in the react-webmapjs tools.
  - WMS GetCapabilities:    ``curl -kL "https://`hostname`/adaguc-services/wms?source=${encodedfilename}&service=wms&request=getcapabilities"`` 
  - WCS GetCapabilities:    ``curl -kL "https://`hostname`/adaguc-services/wms?source=${encodedfilename}&service=wcs&request=getcapabilities"``
  - WMS GetMap for preview: ``curl -kL "https://`hostname`/adaguc-services/wms?source=${encodedfilename}&service=wms&request=getmap&width=1000&format=image/png&LAYERS=tasmax&title=ECEarth%20Tasmax%20test&CRS=EPSG:4326&" > /tmp/test.png && display /tmp/test.png``
  - Get metadata descriptions about dimensions, variables and their attributes: ``curl -kL "https://`hostname`/adaguc-services/wms?service=metadata&source=${encodedfilename}&request=getmetadata"``


## Starting and using adaguc-services without docker-compose
- go into the repository (e.g. cd adaguc-services)
- export ADAGUCSERVICES_DIR=~/adaguc-services-space
- export ADAGUC_SERVICES_CONFIG=\`pwd`/adaguc-services-config.xml.example
- export ADAGUC_SERVICES_PORT=8091
- export ADAGUC_SERVICES_EXTERNAL_URL=http://\`hostname`:${ADAGUC_SERVICES_PORT}
- ls ${ADAGUC_SERVICES_CONFIG}
- mkdir ${ADAGUCSERVICES_DIR} #Creates a folder where adaguc-services will store its data


Building a package from source with maven and add a item to your basket
- Install maven: sudo apt install maven
- Clone this repository
- cd adaguc-services
- Setup the environment variables as described above. 
- mvn package
- The jar file will be put in the target directory (e.g. target/adaguc-services-1.2.0.jar). Setup environment and do java -jar [jarfile]
- Check if the service runs (e.g. visit http://localhost:8091/basket/list . You might have to add adaguc-services/ before the basket)
- Check if director structure is setup: tree ${ADAGUCSERVICES_DIR}
- Check if basket works:
  - echo "hello!" > ${ADAGUCSERVICES_DIR}/data/adaguc-services-space/myuser/data/hello.txt
  - curl -L http://localhost:8091/basket/list | python -m json.tool

<!--
## Getting development environment running on Ubuntu 18 with VSC TBD
- Install Visual Studio Code (VSC), according to VSC's installation instructions
- Install the Java extension pack and the Docker extension pack in VSC
- Install java 11: sudo apt install openjdk-11-jdk tree
- Clone this repository
- cd adaguc-services
- code .
-->

